package com.tp1.graphic;

import java.awt.*;
import java.awt.event.*;

public class Table extends Frame implements WindowListener {
	public static final int MAX = 5;
	private int NUMBER_GUEST_HUNGRY = 0;
	private int NUMBER_GUEST_WAITING = 0;
	private Point CENTER;
	private Plate PLATES[];
	private Chopstick CHOPS[];
	private boolean CHOPSTICKS_AVAILABILITY[];
	private String WINDOW_TITLE = "Dining Philosophers";
	private int TABLE_SIZE = 200;
	private int NUMBER_GUEST;
	private Color BACKGROUND_COLOR = Color.darkGray;
	private int THINKING_COLOR = -1;

	public Table(int numberGuest) {
		super();

		addWindowListener(this);
		setTitle(this.WINDOW_TITLE);
		setSize(this.TABLE_SIZE, this.TABLE_SIZE);
		setBackground(this.BACKGROUND_COLOR);
		setResizable(false);

		this.NUMBER_GUEST = numberGuest;
		int TABLE_HALF_WIDTH = this.getSize().width / 2;
		int TABLE_HALF_HEIGHT = this.getSize().height / 2;
		this.CENTER = new Point(TABLE_HALF_WIDTH, TABLE_HALF_HEIGHT);

		this.setupChopstickAvalaibility();

		int defaultAngle = 360 / numberGuest;
		this.setupPlates(defaultAngle);
		this.setupChopsticks(defaultAngle);

		show();

	}

	private void setupChopsticks(int defaultAngle) {
		this.CHOPS = new Chopstick[this.NUMBER_GUEST];
		for (int i = 0; i < this.NUMBER_GUEST; i++) {
			this.CHOPS[i] = new Chopstick(i, this.CENTER, new Point(this.CENTER.x, this.CENTER.y - 70),
					new Point(this.CENTER.x, this.CENTER.y - 40), defaultAngle);
		}
	}

	private void setupPlates(int defaultAngle) {
		this.PLATES = new Plate[this.NUMBER_GUEST];
		for (int i = 0; i < this.NUMBER_GUEST; i++) {
			this.PLATES[i] = new Plate(i, this.CENTER, new Point(this.CENTER.x, this.CENTER.y - 70), 20, defaultAngle);
		}
	}

	private void setupChopstickAvalaibility() {
		this.CHOPSTICKS_AVAILABILITY = new boolean[this.NUMBER_GUEST];
		for (int i = 0; i < this.NUMBER_GUEST; i++)
			this.CHOPSTICKS_AVAILABILITY[i] = true;
	}

	public void windowOpened(WindowEvent event) {
	}

	public void windowClosing(WindowEvent event) {
		System.exit(0);
	}

	public void windowClosed(WindowEvent event) {
	}

	public void windowIconified(WindowEvent event) {
	}

	public void windowDeiconified(WindowEvent event) {
	}

	public void windowActivated(WindowEvent event) {
	}

	public void windowDeactivated(WindowEvent event) {
	}

	public synchronized void becomesHungry(int philosopherID) {
		while (this.NUMBER_GUEST_HUNGRY == MAX || this.NUMBER_GUEST_WAITING > 0) {
			doWaitGuest();
		}
		this.NUMBER_GUEST_HUNGRY++;
		this.PLATES[philosopherID].setColor(philosopherID);
		repaint();
	}

	private void doWaitGuest() {
		try {
			this.NUMBER_GUEST_WAITING++;
			wait();
			this.NUMBER_GUEST_WAITING--;
		} catch (InterruptedException exception) {
			System.err.println("!!!ERROR DETECTED!!!\nCause : " + exception.getCause() + "\nMessage : "
					+ exception.getMessage() + "\nStack : \n");
			exception.printStackTrace(System.err);
		}
	}

	public synchronized void doThinkingGuest(int philosopherID) {
		this.PLATES[philosopherID].setColor(this.THINKING_COLOR);
		repaint();
		this.NUMBER_GUEST_HUNGRY--;
		notify();
	}

	public synchronized void waitForChopsticksGuest(int leftChopstickID, int rightChopstickID) {
		while (!this.CHOPSTICKS_AVAILABILITY[leftChopstickID] || !this.CHOPSTICKS_AVAILABILITY[rightChopstickID]) {
			try {
				wait();
			} catch (InterruptedException exception) {
				System.err.println("!!!ERROR DETECTED!!!\nCause : " + exception.getCause() + "\nMessage : "
						+ exception.getMessage() + "\nStack : \n");
				exception.printStackTrace(System.err);
			}
		}
		this.CHOPSTICKS_AVAILABILITY[leftChopstickID] = false;
		this.CHOPSTICKS_AVAILABILITY[rightChopstickID] = false;
	}

	public synchronized void notifyReleaseChopstick(int chopstickID) {
		this.CHOPSTICKS_AVAILABILITY[chopstickID] = true;
		notifyAll();

	}

	public void repaintChopstickGuest(int philosopherID, int chopstickID) {
		System.out.println(String.format("repainting ph %d - color %d ", philosopherID, chopstickID));
		this.CHOPS[chopstickID].setColor(philosopherID);
		repaint();
	}

	public void repaintChopstick(int chopstickID) {
		this.CHOPS[chopstickID].setColor(-1);
		repaint();
	}

	public void paint(Graphics graphic) {
		for (int i = 0; i < this.NUMBER_GUEST; i++) {
			this.PLATES[i].draw(graphic);
			this.CHOPS[i].draw(graphic);
		}
	}
}
