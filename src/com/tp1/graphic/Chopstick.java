package com.tp1.graphic;

import java.awt.*;

class Chopstick {
	private Point TABLE_CENTER;
	private Point COORDINATE_START;
	private Point COORDINATE_END;
	private Color COLOR;
	private int ID;
	private int ANGLE;
	private int PHILOSOPHER_ID;

	Chopstick(int ID, Point tableCenter, Point coordianteStart, Point coordianteEnd, int defaultAngle) {
		this.ID = ID;
		this.TABLE_CENTER = tableCenter;
		this.PHILOSOPHER_ID = -1;
		setColor(this.PHILOSOPHER_ID);

		this.ANGLE = defaultAngle * ID + 36;
		this.COORDINATE_START = new Point(
				rotate(coordianteStart.x, coordianteStart.y, tableCenter.x, tableCenter.y, this.ANGLE));
		this.COORDINATE_START.y += 15;

		this.COORDINATE_END = new Point(
				rotate(coordianteEnd.x, coordianteEnd.y, tableCenter.x, tableCenter.y, this.ANGLE));
		this.COORDINATE_END.y += 15;
	}

	protected void draw(Graphics graphic) {
		graphic.setColor(this.COLOR);
		graphic.drawLine(this.COORDINATE_START.x, this.COORDINATE_START.y, this.COORDINATE_END.x,
				this.COORDINATE_END.y);
	}

	protected void setColor(int philosopherID) {
		this.PHILOSOPHER_ID = philosopherID;

		if (philosopherID == -1) {
			this.COLOR = Color.black;
		} else if (philosopherID == 0) {
			this.COLOR = Color.red;
		} else if (philosopherID == 1) {
			this.COLOR = Color.blue;
		} else if (philosopherID == 2) {
			this.COLOR = Color.green;
		} else if (philosopherID == 3) {
			this.COLOR = Color.yellow;
		} else if (philosopherID == 4) {
			this.COLOR = Color.white;
		} else if (philosopherID == 5) {
			this.COLOR = Color.orange;
		}
	}

	private Point rotate(int x, int y, int bX, int bY, int angle) {
		Point p = new Point();
		double a = Math.PI / (180.0 / angle);
		p.x = (int) (x * Math.cos(a) - y * Math.sin(a) - Math.cos(a) * bX + Math.sin(a) * bY + bX);
		p.y = (int) (x * Math.sin(a) + y * Math.cos(a) - Math.sin(a) * bX - Math.cos(a) * bY + bY);

		return (p);
	}
}
