package com.tp1.graphic;

import java.awt.*;

class Plate {
	private Point TABLE_CENTER;
	private Point COORDINATES;
	private Color COLOR;
	private int PHILOSOPHER_ID;
	private int ANGLE;
	private int SIZE;
	private int INDENTATION;

	private Point move(int x, int y, int bX, int bY, int angle) {
		double rotation = Math.PI / (180.0 / angle);
		Point p = new Point();
		p.y = (int) (x * Math.sin(rotation) + y * Math.cos(rotation) - Math.sin(rotation) * bX - Math.cos(rotation) * bY
				+ bY);
		p.x = (int) (x * Math.cos(rotation) - y * Math.sin(rotation) - Math.cos(rotation) * bX + Math.sin(rotation) * bY
				+ bX);

		return (p);
	}

	Plate(int identation, Point tableCenter, Point coordinates, int size, int defaultAngle) {
		this.INDENTATION = identation;
		this.TABLE_CENTER = tableCenter;
		this.SIZE = size;

		this.PHILOSOPHER_ID = -1;
		setColor(this.PHILOSOPHER_ID);

		this.ANGLE = defaultAngle * identation;
		this.COORDINATES = new Point(move(coordinates.x, coordinates.y, tableCenter.x, tableCenter.y, this.ANGLE));
		this.COORDINATES.x -= 10;
		this.COORDINATES.y += 5;
	}

	protected void setColor(int philosopherID) {
		this.PHILOSOPHER_ID = philosopherID;

		if (philosopherID == -1) {
			this.COLOR = Color.black;
		} else if (philosopherID == 0) {
			this.COLOR = Color.red;
		} else if (philosopherID == 1) {
			this.COLOR = Color.blue;
		} else if (philosopherID == 2) {
			this.COLOR = Color.green;
		} else if (philosopherID == 3) {
			this.COLOR = Color.yellow;
		} else if (philosopherID == 4) {
			this.COLOR = Color.white;
		} else if (philosopherID == 5) {
			this.COLOR = Color.orange;
		}
	}

	protected void draw(Graphics graphic) {
		graphic.setColor(this.COLOR);
		graphic.fillOval(this.COORDINATES.x, this.COORDINATES.y, this.SIZE, this.SIZE);
	}

}
