package com.tp1.bean;

import java.util.ArrayList;
import java.util.List;

import com.tp1.graphic.Table;

public class DiningPhilosophers {

	private static List<Philosopher> LIST_GUEST;
	private static Table TABLE;
	private static int NUMBER_GUEST = 5;

	private static void setupDiningPhilosophers() {
		TABLE = new Table(NUMBER_GUEST);
		LIST_GUEST = new ArrayList<Philosopher>();
		for (int i = 0; i < NUMBER_GUEST; i++) {
			if (i == 0)
				LIST_GUEST.add(new Philosopher(i, TABLE, i, NUMBER_GUEST - 1));
			else
				LIST_GUEST.add(new Philosopher(i, TABLE, i, i - 1));
		}
	}

	private static void startDiningPhilosophers() {
		for (Philosopher philosopher : LIST_GUEST)
			philosopher.start();
	}

	public static void main(String args[]) {
		setupDiningPhilosophers();
		startDiningPhilosophers();
	}
}
