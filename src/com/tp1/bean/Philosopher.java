package com.tp1.bean;

import com.tp1.graphic.Table;

public class Philosopher extends Thread {
	final int TIME_THINK_MAX = 4000;
	final int TIME_NEXT_FORK = 1000;
	final int TIME_EAT_MAX = 3000;
	private Table TABLE;
	private int LEFT;
	private int RIGHT;
	private int ID;

	Philosopher(int ID, Table table, int left, int right) {
		this.ID = ID;
		this.TABLE = table;
		this.LEFT = left;
		this.RIGHT = right;
		setName("Philosopher " + ID);
	}

	public void run() {
		// doSleep(500l);
		while (true) {

			this.doThinking();

			this.doEat();

			this.doReleaseChopsticks();
		}
	}

	private void doThinking() {
		this.TABLE.doThinkingGuest(ID);
		System.out.println(getName() + " thinks");
		doSleep((long) (Math.random() * this.TIME_THINK_MAX));
		System.out.println(getName() + " finished thinking");
	}

	private void doEat() {
		System.out.println(getName() + " is hungry");
		this.TABLE.becomesHungry(ID);

		this.getChopsticks();

		doSleep(this.TIME_NEXT_FORK);

		final double timeToSleep = Math.random() * this.TIME_EAT_MAX;
		System.out.println(getName() + " eats for " + timeToSleep);
		doSleep((long) timeToSleep);

		System.out.println(getName() + " finished eating");
	}

	private void doReleaseChopsticks() {
		this.TABLE.repaintChopstick(this.LEFT);
		this.TABLE.notifyReleaseChopstick(this.LEFT);
		System.out.println(getName() + " released left chopstick");

		this.TABLE.repaintChopstick(this.RIGHT);
		this.TABLE.notifyReleaseChopstick(this.RIGHT);
		System.out.println(getName() + " released right chopstick");
	}

	private void getChopsticks() {
		System.out.println(getName() + " wants both chopsticks");
		this.TABLE.waitForChopsticksGuest(this.LEFT, this.RIGHT);
		this.TABLE.repaintChopstickGuest(this.ID, this.LEFT);
		this.TABLE.repaintChopstickGuest(this.ID, this.RIGHT);
		System.out.println(getName() + " got both chopstick");
	}

	private void doSleep(long millisseconds) {
		try {
			sleep(millisseconds);

		} catch (InterruptedException exception) {
			System.err.println("!!!ERROR DETECTED!!!\nCause : " + exception.getCause() + "\nMessage : "
					+ exception.getMessage() + "\nStack : \n");
			exception.printStackTrace(System.err);
		}
	}

}
